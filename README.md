# Utils
La finalidad de esta librería es proveer de las configuraciones globales a todas las apis. 
## MongoDB
## Puertos
## Modelos de Mongoose

# Modo de Instalación
Todas las apis tienen la dependencia en el `package.json` a este repositorio.
Al ser privado, se necesitan credenciales ssh para acceder. 
## Configure las credenciales
En `~/.ssh/` cree el archivo `config` y agregue las siguientes líneas
```
Host gitlab.com/torbichords/
    HostName gitlab.com
    IdentityFile ~/.ssh/id_rsa
    User git
```

En `IdentityFile` debe poner la ruta al `ssh` que corresponde a sus credenciales para el proyecto.

## Verifique la configuración
Para saber si está todo debidamente configurado, ejecute `npm install` en alguna de las apis que utilizan este repositorio.
