'use strict'
let mongoose = require('mongoose');

let connect = (dbConf) =>{
	let connectionString = `mongodb://${dbConf.host}:${dbConf.port}/${dbConf.dbName}`

	if(dbConf.options){
		let keys = Object.keys(dbConf.options)
		connectionString += '?'

		keys.forEach((key, i) => {
			connectionString += `${key}=${dbConf.options[key]}`
			connectionString += i < keys.length -1 ? '&' : ''
		})
	}

	console.log(`MongoDB Connection: ${connectionString}`)

	mongoose.connect(connectionString);
	let db = mongoose.connection; 

	db.on(
		'error', (err) => {
			console.error(err.message, 'retry...')
			setTimeout(() => mongoose.connect(connectionString), 3000)
		}
	);

	db.on('open', function() {
		console.log(`connected to database ${connectionString}`)
	});

}

module.exports = {
	connect 			: connect
};
