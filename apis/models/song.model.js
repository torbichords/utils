'use strict'
let mongoose = require('mongoose')
var Schema = mongoose.Schema

let Chord = mongoose.Schema({
    chord       : String,
    position    : Number
}, { _id: false})

let SongLine = mongoose.Schema({
    lyric   : String,
    chords  : [Chord]
}, { _id: false})

let Section = mongoose.Schema({
    name    : String,
    lines   : [SongLine]
}, { _id: false})



let song = mongoose.Schema({
    "title"           : { type : String, required: true},
    "simplifiedTitle" : { type : String, required: true},
    "author"          : { type: Schema.Types.ObjectId, ref: 'Artist', required: true },
    "transcriptor"    : { type: Schema.Types.ObjectId, ref: 'User' },
    "song"            : [ Section ],
    "tonality"        : { type : String, required: true}
})
song.index({ simplifiedTitle : "text" })
// song.index({ author : 1, titleSimplified : 1}, { unique : true })

module.exports = mongoose.model('Song', song)
