'use strict'
let mongoose = require('mongoose')
var Schema = mongoose.Schema

let userSubscription = mongoose.Schema({
    "user"       : {type: Schema.Types.ObjectId, ref: 'User' },
    "favorites" : [{type: Schema.Types.ObjectId, ref: 'Song' }],
    "recents"    : [{type: Schema.Types.ObjectId, ref: 'Song' }],
})

userSubscription.index({
    user : 1
}, {
    unique:true
})

module.exports = mongoose.model('UserSubscription', userSubscription)
