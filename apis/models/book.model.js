'use strict'
let mongoose = require('mongoose')
var Schema = mongoose.Schema
let book = mongoose.Schema({
    "title"         : {type: String, required : true },
    "description"   : String,
    "removable"     : { type : Boolean, default : true},
    "songs"         : [{type: Schema.Types.ObjectId, ref: 'Song'}],
    "author"        : {type: Schema.Types.ObjectId, ref: 'User', required : true },
    "participants"  : [{type: Schema.Types.ObjectId, ref: 'User'}]
}, { timestamps : true })

module.exports = mongoose.model('Book', book)