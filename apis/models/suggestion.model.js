'use strict'
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var suggestionSchema = mongoose.Schema({
  user         : { type: Schema.Types.ObjectId, ref: 'User'},
  title        : { type : String, required : true },
  description  : { type : String, required : true },
  date         : { type : Date, default : new Date().getTime() },
  status       : {
    _id : false,
    fixVersion  : String,
    resolved    : { type : Boolean, default : false},
    mustResolve : Boolean,
    date        : Date
  }
})

module.exports = mongoose.model('Suggestion', suggestionSchema)