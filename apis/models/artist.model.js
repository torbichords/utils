'use strict'
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var artistSchema = mongoose.Schema({
  name: {type : String, required : true},
  simplifiedName : { type : String, required : true}
})

artistSchema.index({ simplifiedName : "text"})
artistSchema.index({ name : 1}, {unique : true})
artistSchema.index({ simplifiedName : 1}, {unique : true})
module.exports = mongoose.model('Artist', artistSchema)