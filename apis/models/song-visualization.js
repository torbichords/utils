'use strict'
let mongoose = require('mongoose')
var Schema = mongoose.Schema

let songVisualization = mongoose.Schema({
    "song"             : { type : Schema.Types.ObjectId, ref : 'Song', required : true },
    "user"             : { type : Schema.Types.ObjectId, ref : 'User', required : true },
    "book"             : { type : Schema.Types.ObjectId, ref : 'Book', required : true },
    "tonality"         : String,
    "columns"          : Boolean,
    "chords"           : {
        notation : String,
        show : Boolean,
        open : Boolean,
        simplify: String,
        text : {
            bold : Boolean,
            size : String,
            color : String
        }
    },
    text : {
        bold : Boolean,
        size : String,
        color : String
    },
    collapsablePanels : {
        text : Boolean,
        chords : Boolean
    },
    // "fontSize"         : Number,
    // "marginSize"       : Number,
    // "hideNotes"        : Boolean,
    // "lyricLineChanges" : [{
    //     section: Number,
    //     line   : Number,
    //     change : String
    // }],
    // "chordLineChanges" : [{
    //     section : Number,
    //     line    : Number,
    //     change  : [{
    //         chord    : String,
    //         position : Number
    //     }]
    // }],
    "notes" : Boolean
    // "notes" : [{
    //     section  : Number,
    //     line     : Number,
    //     collapse : Boolean,
    //     title    : String,
    //     content  : String,
    //     noteType : Number
    // }]
})

songVisualization.index({
    song : 1,
    book : 1
}, {
    unique:true
})

module.exports = mongoose.model('SongVisualization', songVisualization)
