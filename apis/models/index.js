let mongoose = require('mongoose')
mongoose.Promise = require('q').Promise
module.exports = {
    User                : require('./user.model'),
    Book                : require('./book.model'),
    Song                : require('./song.model'),
    Artist              : require('./artist.model'),
    SongVisualization   : require('./song-visualization'),
    UserSubscription    : require('./user-subscription'),
    Suggestion          : require('./suggestion.model')
}