'use strict'
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var userSchema = Schema({
  username : {type : String, required : true},
  email    : {type : String, required : true},
  password : {type : String, required : true},
  status   : {type : Number},
  contacts : [{
    user   : { type : Schema.Types.ObjectId, ref: 'User' },
    status : { type : String, enum : ['ACCEPTED', 'PENDING', 'REJECTED', 'BLOCKED'], default : 'PENDING'}
  }],
  notifications : [{
    _type : { type : String, enum : [
      'CONTACT_REQUEST', 
      'CONTACT_ACCEPTED', 
      'CONTACT_REJECTED'
    ]},
    body  : { type : Object }
  }]
}, { toJSON: { virtuals: true }, usePushEach: true })

userSchema.virtual('books', {
  ref          : 'Book',
  localField   : '_id',
  foreignField : 'author'
})

userSchema.virtual('sharedBooks', {
  ref          : 'Book',
  localField   : '_id',
  foreignField : 'participants'
})

userSchema.virtual('subscriptions', {
  ref          : 'UserSubscription',
  localField   : '_id',
  foreignField : 'user'
})

userSchema.index({ email : 1},{ unique : true })
module.exports = mongoose.model('User', userSchema)