const { createLogger, format, transports, add } = require('winston');
const winston = require('winston');
const { combine, timestamp, label, printf, prettyPrint } = format;
var _appName
var _logger

const elasticsearch = require('./configs').elasticsearch
const logstash = require('./configs').logstash

const addAppName = format((info, opts) => {
    info.appName = _appName
    return info
})

function config(appName) {
    console.log(`config logger for ${appName}. Logstash Host ${logstash.host}`)
    _appName = appName
    const logger = createLogger({
        transports: [
            new transports.Console(),
            new transports.Http({
                host: logstash.host,
                port: logstash.port,
                auth: {
                    username: elasticsearch.username,
                    password: elasticsearch.password
                }
            })
        ],
        format: combine(
            addAppName(),
            timestamp(),
            prettyPrint()
        )
    });
    _logger = logger
    return logger
}

module.exports = {
    config : function (appName) {
        let logger = _logger
        if (!logger) {
            logger = config(appName)
        }
        return logger
    },
    info : (data) => {
        _logger.info(data)
    },
    warn : (data) => {
        _logger.warn(data)
    },
    error : (data) => {
        _logger.error(data)
    }
}

