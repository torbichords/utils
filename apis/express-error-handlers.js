function getRequestObject(req){
  return {
      method  : req.method,
      url     : req.url,
      body    : req.body,
      query : req.query
    }
}


exports.handler = {
    devHandler : function (err, req, res, next){
        const logger = require('./logger')
        err.status = err.status || 500
        err.request = getRequestObject(req)
    
        logger.error(err.message, err)
        res.status(err.status);
        res.send(err);
    },
    prodHandler : function (err, req, res, next){
        const logger = require('./logger')
        err.status = err.status || 500
        err.request = getRequestObject(req)
        logger.error(err.message, err)
        res.status(err.status);
        res.send({
          status : err.status,
          error : err
        });
    } 
}