'use strict'

let nodeEnv = process.env.NODE_ENV || 'dev'

let config = {
    dev    : require('./dev.config.json'),
    int    : require('./int.config.json'),
    pre    : require('./pre.config.json'),
    prod   : require('./prod.config.json'),
	docker : require('./docker.config.json')
}

module.exports = config[nodeEnv] || config.dev