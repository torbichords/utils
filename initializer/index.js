module.exports = function(apiName, routes, apiVersion){
  let logger = require('../index').logger

  logger.config(`api-${apiName}`)
  
  var app = require('./app')(apiName, routes, apiVersion || 'no-versioned');
  var debug = require('debug')(`api-${apiName}:server`);
  var http = require('http');
  
  var apiPort = require('../').config.apis[apiName].port;
  
  logger.info(`api-${apiName} port ${apiPort}`)
  
  var port = normalizePort(process.env.PORT || apiPort);
  app.set('port', port);
  
  var server = http.createServer(app);
  
  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);
  
  
  function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
  }
  
  function onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }
  
    var bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;
  
    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        logger.error.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        logger.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }
  
  function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port;
    debug('Listening on ' + bind);
  }
}