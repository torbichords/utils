'use strict'

module.exports = function (apiName, routes, apiVersion) {
  const utils = require('../')
  const logger = utils.logger

  logger.config(`api-${apiName}`)

  var express = require('express');
  var path = require('path');
  var cookieParser = require('cookie-parser');
  var bodyParser = require('body-parser');

  utils.connect(utils.config.database)

  var app = express();

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());

  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.use((req, res, next) => {
    if(!req.url.includes('health') && req.url.includes('api')){
      logger.info(`${req.method} - ${req.url}`, {
        body: req.body,
        query: req.query,
        url: req.url,
        method: req.method,
        headers: req.rawHeaders,
        hostname: req.hostname,
        ip: req.ip,
        originalUrl: req.originalUrl
      })
    }    
    next()
  })

  routes.forEach(route => {
    logger.info(`config route ${route.endpoint}`)
    app.use(route.endpoint + '/healthz', (req, res) => res.send(health(apiName, req, apiVersion)))
    app.use(route.endpoint, route.router);
  })

  app.get('/', (req, res) => {
    res.send(health(apiName, req, apiVersion))
  })

  app.get('/version', (req, res) => res.send({
    api : apiName,
    version : apiVersion
  }))

  app.get('/healthz', (req, res) => {
    res.send(health(apiName, req, apiVersion))
  })

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  const errorHandler = require('../apis/express-error-handlers').handler
  if (app.get('env') === 'development') {
    app.use(errorHandler.devHandler);
  } else {
    app.use(errorHandler.prodHandler);
  }

  return app
};


function health(apiName, req, apiVersion) {
  return {
    status: 'OK',
    datetime: new Date(),
    api: `api-${apiName}`,
    headers: req.rawHeaders,
    hostname: req.hostname,
    ip: req.ip,
    originalUrl: req.originalUrl,
    version : apiVersion
  }
}